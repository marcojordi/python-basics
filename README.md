# Python Basics
This repository contains various basic programming examples with Python 3.7.xx

- Jupyter - Introduction
- Python Basics
- Python Packages
- Advanced Python Stuff
- Git
- Python Cheatsheets


## Credits 
* https://gist.github.com/kenjyco/69eeb503125035f21a9d
* https://github.com/jerry-git/learn-python3